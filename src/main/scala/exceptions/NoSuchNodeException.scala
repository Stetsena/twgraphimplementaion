package exceptions

/**
  * Created by andrew on 5/10/17.
  */
object NoSuchNodeException {
  def apply(message: String = "NO SUCH NODE"): NoSuchNodeException = new NoSuchNodeException(message)
}

class NoSuchNodeException (message: String = "NO SUCH NODE") extends Exception(message)
