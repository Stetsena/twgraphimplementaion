
import exceptions.WrongGraphInitialisationInput
import graph.Graph

import scala.io.StdIn.readLine
import filters.FilterFunctions._
import filters.ReturnFunctions._


/**
  * Created by andrew on 5/7/17.
  */
object Controller {

  def main(args: Array[String]): Unit = {

    val distance = "The distance of the route (([A-Z]-?)+).?".r
    val maxDistance = "The number of different routes from ([A-Z]) to ([A-Z]) with a distance of less than ([0-9]{1,5})[.]?".r
    val exactNumberOfPoints = "The number of trips starting at ([A-Z]) and ending at ([A-Z]) with exactly ([0-9]{1,5}) stops[.]?".r
    val maxNumberOfPoints = "The number of trips starting at ([A-Z]) and ending at ([A-Z]) with a maximum of ([0-9]{1,5}) stops[.]?".r
    val shortestPath = "The length of the shortest route from ([A-Z]) to ([A-Z])[.]?".r

    if(!args.mkString.matches("(?:[A-Z][A-Z]\\d+[,]?)+")){
     throw WrongGraphInitialisationInput()
    }

    var runningState = true
    val graph : Graph = Graph(args)


    while (runningState) {
      val line: String = readLine("[Please input command] >> ")
      if(!line.isEmpty) {
        try {
          line match {
            case distance(route, last) => {
              val listOfPoints: List[String] = route.split("-").map(_.replace("-", "")).toList
              println("Response -> " + graph.getRouteDistance(listOfPoints))
            }
            case maxNumberOfPoints(from, to, number) => {
              println("Response -> " + graph.getGraphRoutesWithConditions(from, to, limitByMaxStops(number.toInt) _)(getListSize))
            }
            case exactNumberOfPoints(from, to, number) => {
              println("Response -> " + graph.getGraphRoutesWithConditions(from, to, limitByExactStops(number.toInt) _)(getListSize))
            }
            case maxDistance(from, to, number) => {
              println("Response -> " + graph.getGraphRoutesWithConditions(from, to, limitByMaxDistance(number.toInt) _)(getListSize))
            }
            case shortestPath(from, to) => {
              println("Response -> " + graph.getGraphRoutesWithConditions(from, to, limitByShortestDistance(Int.MaxValue) _)(getRouteDistance))
            }
            case "quit" => {
              runningState = false
            }
            case _ => println("Unknown Command")
          }
        }catch {
          case ex: Throwable => println("Response -> "+ex.getMessage)
        }
      }
    }

  }
}
