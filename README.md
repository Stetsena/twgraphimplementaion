# TW Graph Project
Implemented and ready for use

### Prerequisites

Please make sure you have latest SBT installed on your environment!


### Implementation Details

1) Solution not using any frameworks apart of that was used for testing
2) I prefer scala because it can reduce boilerplate code

### Running

When you have your SBT installed please in terminal open Project folder
and execute next command start command line (You can change input Graph as you want, just follow the style):

```
sbt "run AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7"

```

You should see following:

```
[Please input command] >> 

```

Now you should be able to execute commands, while list examples of possible commands is:

```
[Please input command] >> The distance of the route A-B-C
[Please input command] >> The number of trips starting at C and ending at C with a maximum of 3 stops
[Please input command] >> The number of trips starting at A and ending at C with exactly 4 stops
[Please input command] >> The length of the shortest route from A to C
[Please input command] >> The number of different routes from C to C with a distance of less than 30

```

*Note
You could change the parameters in command, but cant change the format, other way you will have "Unknown command" warning.

### Testing

In order to run unit testing please execute next command:

```
sbt test

```