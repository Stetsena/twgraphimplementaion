package graph

/**
  * Created by andrew on 5/7/17.
  */
object GraphNode {
  def apply(cityName: String): GraphNode = new GraphNode(cityName)
}

class GraphNode(val cityName: String) {

  var incommingDistanceMap: Map[String, Int] = Map()

  var outgoing: Map[String, GraphNode] = Map()

  def addIncomming(cityName: String, distance: Int): GraphNode = {
    incommingDistanceMap = incommingDistanceMap + (cityName -> distance)
    this
  }

  def addOutgoing(node : GraphNode): GraphNode = {
    outgoing = outgoing + (node.cityName -> node)
    this
  }




  def canEqual(other: Any): Boolean = other.isInstanceOf[GraphNode]

  override def equals(other: Any): Boolean = other match {
    case that: GraphNode =>
      (that canEqual this) &&
        cityName == that.cityName
    case _ => false
  }

  override def hashCode(): Int = {
    val state = Seq(cityName)
    state.map(_.hashCode()).foldLeft(0)((a, b) => 31 * a + b)
  }
}
