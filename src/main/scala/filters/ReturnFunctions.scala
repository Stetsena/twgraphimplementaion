package filters

import graph.GraphNode
import graph.Graph.routeDistance


import scala.collection.mutable.MutableList

/**
  * Created by andrew on 5/9/17.
  */
object ReturnFunctions {

  def getRouteDistance(routes: MutableList[List[GraphNode]]): Int = {
    routeDistance(routes.head)
  }

  def getListSize(routes: MutableList[List[GraphNode]]): Int = {
    routes.size
  }
}
