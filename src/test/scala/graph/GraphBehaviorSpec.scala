package graph

import exceptions.{NoSuchNodeException, NoSuchRouteException}
import filters.FilterFunctions.{limitByExactStops, limitByMaxDistance, limitByMaxStops, limitByShortestDistance}
import filters.ReturnFunctions.{getListSize, getRouteDistance}
import org.scalatest.{BeforeAndAfterAll, FlatSpec, Matchers}

/**
  * Created by andrew on 5/10/17.
  */
class GraphBehaviorSpec extends FlatSpec with Matchers with BeforeAndAfterAll {

  val graph : Graph = Graph()

  val smallGraph : Graph = Graph()

  val unconnectedGraph : Graph = Graph()

  override def beforeAll {
    "AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7".split("[,]").foreach(trip => {
      graph.addTrip(trip.trim.replace(",", ""))
    })

    "AB5".split("[,]").foreach(trip => {
      smallGraph.addTrip(trip.trim.replace(",", ""))
    })

    "AB5, DC3".split("[,]").foreach(trip => {
      unconnectedGraph.addTrip(trip.trim.replace(",", ""))
    })
  }


  "The distance of the route A-B-C" should "be equals to 9" in {
    graph.getRouteDistance(List("A", "B", "C")) should equal (9)
  }

  "The distance of the route A-D" should "be equals to 5" in {
    graph.getRouteDistance(List("A", "D")) should equal (5)
  }

  "The distance of the route A-D-C" should "be equals to 13" in {
    graph.getRouteDistance(List("A", "D", "C")) should equal (13)
  }

  "The distance of the route A-E-B-C-D" should "be equals to 22" in {
    graph.getRouteDistance(List("A", "E", "B", "C","D")) should equal (22)
  }

  "The distance of the route A-E-D" should "throw NoSuchRouteException" in {
    a [NoSuchRouteException] should be thrownBy { graph.getRouteDistance(List("A", "E","D")) }
  }

  "The number of trips starting at C and ending at C with a maximum of 3 stops" should "be equals to 2" in {
    graph.getGraphRoutesWithConditions("C", "C", limitByMaxStops(3)_)(getListSize) should equal (2)
  }

  "The number of trips starting at A and ending at C with exactly 4 stops" should "be equals to 3" in {
    graph.getGraphRoutesWithConditions("A", "C", limitByExactStops(4)_)(getListSize) should equal (3)
  }

  "The length of the shortest route (in terms of distance to travel) from A to C" should "be equals to 9" in {
    graph.getGraphRoutesWithConditions("A", "C", limitByShortestDistance(Int.MaxValue)_)(getRouteDistance) should equal (9)
  }

  "The length of the shortest route (in terms of distance to travel) from B to B" should "be equals to 9" in {
    graph.getGraphRoutesWithConditions("B", "B", limitByShortestDistance(Int.MaxValue)_)(getRouteDistance) should equal (9)
  }

  "The number of different routes from C to C with a distance of less than 30" should "be equals to 7" in {
    graph.getGraphRoutesWithConditions("C", "C", limitByMaxDistance(30)_)(getListSize) should equal (7)
  }

  "The number of trips with a maximum of 3 stops, if graph doesn't have specified nodes" should "throw NoSuchNodeException" in {
    a [NoSuchNodeException] should be thrownBy { smallGraph.getGraphRoutesWithConditions("C", "C", limitByMaxStops(3)_)(getListSize) }
  }

  "The distance between existing points in graph with one route" should "return distance of single route" in {
    smallGraph.getRouteDistance(List("A", "B")) should equal (5)
  }

  "The distance between non existing points in graph with one route" should "throw NoSuchNodeException" in {
    a [NoSuchNodeException] should be thrownBy { smallGraph.getRouteDistance(List("C", "C")) }
  }

  "The number of trips with a maximum of 3 stops, if graph does not have this route" should "equal to 0" in {
    unconnectedGraph.getGraphRoutesWithConditions("C", "C", limitByMaxStops(3)_)(getListSize) should equal (0)
  }

  "The number of trips with a exact of 3 stops, if graph does not have this route" should "equal to 0" in {
    unconnectedGraph.getGraphRoutesWithConditions("C", "C", limitByExactStops(3)_)(getListSize) should equal (0)
  }

  "The number of trips with a maximum 30 distance, if graph does not have this route" should "equal to 0" in {
    unconnectedGraph.getGraphRoutesWithConditions("C", "C", limitByMaxDistance(30)_)(getListSize) should equal (0)
  }

  "The shortest path for specified nodes, if graph does not have this route" should "equal to 0" in {
    unconnectedGraph.getGraphRoutesWithConditions("C", "C", limitByShortestDistance(Int.MaxValue)_)(getListSize) should equal (0)
  }


}
