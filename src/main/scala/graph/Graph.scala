package graph

import exceptions.{NoSuchNodeException, NoSuchRouteException}

import scala.annotation.tailrec
import scala.collection.mutable.MutableList
import helpers.HelperFunctions.validatePathParams

/**
  * Created by andrew on 5/9/17.
  */
object Graph{
  def apply() : Graph = new Graph()

  def apply(args: Array[String]) : Graph = {
    val graph = new Graph()
    args.foreach(trip => {
      graph.addTrip(trip.trim.replace(",", ""))
    })
    return graph
  }

  val routeDistance = (route: List[GraphNode]) => {
    var totalDist = 0
    var prevCity: GraphNode = route.head
    route.drop(1).foreach(city => {
      if (!prevCity.outgoing.contains(city.cityName)) {
        throw NoSuchRouteException()
      }
      totalDist += city.incommingDistanceMap(prevCity.cityName)
      prevCity = city
    })
    totalDist
  }
}

class Graph {

  var cities: Map[String, GraphNode] = Map()

  def addTrip(inputParam: String): Unit = {
    val from = inputParam.charAt(0).toString
    val to = inputParam.charAt(1).toString
    val dist = inputParam.charAt(2).toString.toInt
    if (!cities.contains(from)) {
      cities = cities + (from -> GraphNode(from))
    }
    if (!cities.contains(to)) {
      cities = cities + (to -> GraphNode(to))
    }
    cities(from).addOutgoing(cities(to))
    cities(to).addIncomming(cities(from).cityName, dist)
  }

  def getGraphRoutesWithConditions (from: String,
                                   to: String,
                                   filterFunction: (String, List[GraphNode], GraphNode, MutableList[List[GraphNode]]) => Option[List[GraphNode]])
                                   (returnFunction: MutableList[List[GraphNode]] => Int): Int = {

    validatePathParams(cities, List(from, to))

    val firstRound: List[List[GraphNode]] = List() :+ List(cities(from))

    val tripsWithLimit = returnRoutesWithLimitation(to, firstRound, MutableList(), filterFunction)

    return returnFunction(tripsWithLimit)
  }


  def getRouteDistance(points: List[String]): Int = {
    validatePathParams(cities, points)
    val route = points.map(cities(_))
    var totalDist = 0
    var prevCity: GraphNode = route.head
    route.drop(1).foreach(city => {
      if (!prevCity.outgoing.contains(city.cityName)) {
        throw NoSuchRouteException()
      }
      totalDist += city.incommingDistanceMap(prevCity.cityName)
      prevCity = city
    })
    totalDist
  }


  @tailrec
  private def returnRoutesWithLimitation(to: String,
                                         listOfRoutes: List[List[GraphNode]],
                                         listOfResults: MutableList[List[GraphNode]],
                                         filterFunction: (String, List[GraphNode], GraphNode, MutableList[List[GraphNode]]) => Option[List[GraphNode]]): MutableList[List[GraphNode]] = {
    if (listOfRoutes.size == 0) {
      return listOfResults
    }

    var nextRound: List[List[GraphNode]] = List()
    var results: MutableList[List[GraphNode]] = listOfResults

    listOfRoutes.foreach(route => {
      route.last.outgoing.values.foreach(point => {
        nextRound = nextRound ++ filterFunction(to, route, point, results)
      })
    })
    returnRoutesWithLimitation(to, nextRound, results, filterFunction)
  }
}
