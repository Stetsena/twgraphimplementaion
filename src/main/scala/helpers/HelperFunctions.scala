package helpers

import exceptions.NoSuchNodeException
import graph.GraphNode

import scala.annotation.tailrec
import scala.collection.mutable.MutableList

/**
  * Created by andrew on 5/10/17.
  */
object HelperFunctions {

  def isValidRoute(to: String, route: List[GraphNode], point: GraphNode) = {
    (route.size != 1 && point.cityName.equals(to))
  }

  def addIfNotExist(lists: MutableList[List[GraphNode]], list: List[GraphNode]) = {
    if (lists.contains(list)) lists else lists += list
  }

  def validatePathParams(cities: Map[String, GraphNode],points: List[String]) = {
    var errorMessage = ""
    points.foreach(point => {
      if (!cities.contains(point)) errorMessage = errorMessage + s"<$point> node does not exist in a graph \n"
    })
    if(!errorMessage.isEmpty) throw NoSuchNodeException(errorMessage)
  }


}
