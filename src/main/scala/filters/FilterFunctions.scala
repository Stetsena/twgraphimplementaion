package filters

import graph.GraphNode
import graph.Graph.routeDistance


import helpers.HelperFunctions._
import scala.collection.mutable.MutableList

/**
  * Created by andrew on 5/9/17.
  */
object FilterFunctions {

  def limitByShortestDistance(limitationParam: Int)(to: String, route: List[GraphNode], point: GraphNode, results: MutableList[List[GraphNode]]): Option[List[GraphNode]] = {
    var limitation = limitationParam
    val distance = routeDistance(route :+ point)
    results.foreach(list => {
      limitation = routeDistance(list)
    })
    if (isValidRoute(to, route, point)) {
      if (distance < limitation) {
        results.clear()
        results += (route :+ point)
      }
    } else if (distance < limitation) {
      return Some(route :+ point)
    }
    None
  }

  def limitByMaxDistance(limitationParam: Int)(to: String, route: List[GraphNode], point: GraphNode, results: MutableList[List[GraphNode]]): Option[List[GraphNode]] = {
    var limitation = limitationParam
    if (isValidRoute(to, route, point) && routeDistance(route :+ point) < limitation) {
      addIfNotExist(results,(route :+ point))
    }
    if (routeDistance(route :+ point) < limitationParam) Some(route :+ point) else None
  }



  def limitByMaxStops(limitationParam: Int)(to: String, route: List[GraphNode], point: GraphNode, results: MutableList[List[GraphNode]]): Option[List[GraphNode]] = {
    var limitation = limitationParam
    if (isValidRoute(to, route, point) && route.size <= limitation) {
      results += (route :+ point)
    }
    if(route.size < limitation) Some(route :+ point) else None
  }


  def limitByExactStops(limitationParam: Int)(to: String, route: List[GraphNode], point: GraphNode, results: MutableList[List[GraphNode]]): Option[List[GraphNode]] = {
    var limitation = limitationParam
    if ((route :+ point).size == limitation + 1) {
      if (isValidRoute(to, route, point)) results += (route :+ point)
      None
    } else {
      Some(route :+ point)
    }
  }



}
