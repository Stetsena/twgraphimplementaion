package exceptions

/**
  * Created by andrew on 5/10/17.
  */
object WrongGraphInitialisationInput {
  def apply(message: String = "Please Provide input in next format(Example) : AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7..."): WrongGraphInitialisationInput = new WrongGraphInitialisationInput(message)
}

class WrongGraphInitialisationInput (message: String = "Please Provide input in next format(Example) : AB5, BC4, CD8, DC8, DE6, AD5, CE2, EB3, AE7...") extends Exception(message)
