package exceptions

/**
  * Created by andrew on 5/10/17.
  */
object NoSuchRouteException {
  def apply(message: String = "NO SUCH ROUTE"): NoSuchRouteException = new NoSuchRouteException(message)
}

class NoSuchRouteException(message: String = "NO SUCH ROUTE") extends Exception(message)
